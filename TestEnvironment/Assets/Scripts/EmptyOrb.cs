using UnityEngine;
using System.Collections;

public class EmptyOrb : MonoBehaviour
{
	public float Mass = 1;
	
	void Awake()
	{
		enabled = false;
		rigidbody.isKinematic = false;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.collider.isTrigger == true || other.gameObject.tag == "MainCamera")
		{
			return;
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
}