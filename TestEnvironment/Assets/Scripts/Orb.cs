using UnityEngine;
using System.Collections;

public class Orb : MonoBehaviour
{
	//public float Mass = 10;
	//private bool changedMass = false;
	public bool isAttached = false;
	private Transform attachedTransform;
	private Vector3 localAttachmentPoint = Vector3.zero;
	
	void Awake()
	{
		enabled = false;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.collider.isTrigger == true || other.gameObject.tag == "MainCamera")
		{
			return;
		}
		else if((this.gameObject.tag == "AttractionOrb" && other.gameObject.tag == "RepulsionOrb") || 
			(other.gameObject.tag == "AttractionOrb" && this.gameObject.tag == "RepulsionOrb"))
		{
			//Debug.Log ("Destroying object");
			Destroy(other.gameObject);
			Destroy(this.gameObject);
			
			GameObject player = GameObject.Find("Player");
			player.GetComponent<OrbFiring>().orbCount -= 2;
		}
		/*else if((this.gameObject.tag == "AttractionOrb" && other.gameObject.tag == "AttractionOrb") || 
			(other.gameObject.tag == "RepulsionOrb" && this.gameObject.tag == "RepulsionOrb"))
		{
			if(isAttached)
			{
				this.gameObject.transform.localScale *= 2;
				SphereCollider collide = (SphereCollider)this.gameObject.collider;
				collide.radius = this.gameObject.transform.localScale.magnitude + .5f;
			}
			else
			{
				Destroy(this.gameObject);
			}
		}*/
		else
		{
			
			this.rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
		
			//Debug.Log ("Collision!");
			if(isAttached)
			{
				return;
			}
			
			isAttached = true;
			
			if(other.rigidbody)
			{
				
				
				this.rigidbody.velocity = Vector3.zero;
				var joint = gameObject.AddComponent<FixedJoint>();
				joint.connectedBody = other.rigidbody;
			}
			else
			{
				
				attachedTransform = other.transform;
							
				localAttachmentPoint = attachedTransform.InverseTransformPoint(transform.position);
				rigidbody.isKinematic = true;
				enabled = true;
				
				
			}
		}
	}
	
	void destroyThis()
	{
		Destroy(this.gameObject);
		//add explosion
		GameObject player = GameObject.Find("Player");
		player.GetComponent<OrbFiring>().orbCount -= 2;
	}
	
	void FixedUpdate ()
	{
		if(attachedTransform)
		{
			transform.position = attachedTransform.TransformPoint(localAttachmentPoint);
		}
		else
		{
			rigidbody.isKinematic = false;
			enabled = false;
		}
	
	}
	
	// Update is called once per frame
	/*void Update ()
	{
		if(attachedTransform)
		{
			transform.position = attachedTransform.TransformPoint(localAttachmentPoint);
		}
		else
		{
			rigidbody.isKinematic = false;
			enabled = false;
		}
	
	}*/
	
	
	
	
}

