using UnityEngine;
using System.Collections;

//Extend this class if the player can interact with this object through the interaction key.
//The object must implement a collider with the option isTrigger activated.
public abstract class InteractiveObject : MonoBehaviour
{

    private bool reachable = false;
    public Texture2D button;

    void Update()
    {
        if (reachable && Input.GetKeyDown(KeyCode.E))
        {
            reachable = false;
            InteractionStarted();
        }
    }

    void OnGUI()
    {
        if (reachable)
            GUI.Label(new Rect(Screen.width / 2 - 37, Screen.height / 2 - 37, 75, 75), button);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "MainCamera")
            reachable = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "MainCamera")
            reachable = false;
    }

    abstract public void InteractionStarted();
}
