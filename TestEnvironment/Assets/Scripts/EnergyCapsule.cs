using UnityEngine;
using System.Collections;

public class EnergyCapsule : MonoBehaviour {

	public int energy = 30;
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "MainCamera")
		{
			GameObject player = GameObject.Find("Player");
			player.SendMessage("addEnergy", energy);
			/*if(this.gameObject.transform.parent.gameObject.name == "EnergyCapsule Circle")
			{
				Destroy(this.gameObject.transform.parent.gameObject);
			}
			else
			{
				Destroy(this.gameObject);
			}*/
			Destroy(this.gameObject);
		}
	}
}
