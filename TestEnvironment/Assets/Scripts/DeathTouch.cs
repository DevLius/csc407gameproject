using UnityEngine;
using System.Collections;

public class DeathTouch : MonoBehaviour {
	
	void OnCollisionEnter (Collision col)
	{
		Collider other = col.collider;
		if(other.gameObject.tag == "MainCamera")
		{
			Player player = other.GetComponent("Player") as Player;
			player.dead = true;
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "MainCamera")
		{
			Player player = other.GetComponent("Player") as Player;
			player.dead = true;
		}
		
	}
}
