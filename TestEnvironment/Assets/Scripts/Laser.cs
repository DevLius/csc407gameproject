using UnityEngine;
using System.Collections;
     
[RequireComponent(typeof(LineRenderer))]
     
public class Laser : MonoBehaviour
{
	Vector2 mouse;
	RaycastHit hit;
	LineRenderer line;
    public Material blueLaser;
	public Material redLaser;
	 
    void Start()
    {
	    line = GetComponent<LineRenderer>();
	    line.SetVertexCount(2);
	    line.SetWidth(0.1f, 0.1f);
    }
     
    void Update()
	{
		if(Input.GetMouseButtonDown(0) && GameObject.Find("Player").GetComponent<OrbFiring>().orbCount < GameObject.Find("Player").GetComponent<OrbFiring>().maxOrbCount)
	    {
			RaycastHit hit = new RaycastHit();
			line.renderer.material = blueLaser;
			
	    	if(Physics.Raycast(transform.position, transform.forward, out hit))
	    	{
		    	line.enabled = true;
		    	line.SetPosition(0, transform.position);
		    	line.SetPosition(1, hit.point);
				StartCoroutine(Timer());
	    	}
	    } 
		
		else if (Input.GetMouseButtonDown(1) && GameObject.Find("Player").GetComponent<OrbFiring>().orbCount < GameObject.Find("Player").GetComponent<OrbFiring>().maxOrbCount)
		{
			RaycastHit hit = new RaycastHit();
			line.renderer.material = redLaser;
			
	    	if(Physics.Raycast(transform.position, transform.forward, out hit))
	    	{
		    	line.enabled = true;
		    	line.SetPosition(0, transform.position);
		    	line.SetPosition(1, hit.point);
				StartCoroutine(Timer());
	    	}
		}
    }

	IEnumerator Timer()
	{
		yield return new WaitForSeconds(0.05f);
		line.enabled = false;
	}

}