using UnityEngine;
using System.Collections;

public abstract class Activator : InteractiveObject
{
	public float duration = -1;

    abstract public void SetActivated(bool active);

    abstract override public void InteractionStarted();

}