using UnityEngine;
using System.Collections;

public class OrbFiring : MonoBehaviour {
	
	public GameObject AttractionOrb;
	public GameObject RepulsionOrb;
	public GameObject EmptyOrb;
	public GameObject EmptyOrb2;
	public float FireSpeed = 1000;
	public int maxOrbCount = 5;
	public int orbCount = 0;
	public int timeToLive = 3;
	public AudioClip laser;
	private GameObject orb;
	
	void Update ()
	{
		if(Input.GetMouseButtonDown(0) && orbCount < maxOrbCount)
		{
			audio.PlayOneShot(laser);
			
			//GameObject emptyOrb = (GameObject)Instantiate(EmptyOrb, transform.position, Quaternion.identity);
			//emptyOrb.rigidbody.velocity = transform.forward * FireSpeed;
			
			RaycastHit hit = new RaycastHit();
				if (Physics.Raycast(transform.position, transform.forward, out hit))
				{
	        		var rot = Quaternion.FromToRotation(Vector3.up, hit.normal);
	        		if (hit.transform.tag == "RepulsionOrb")
					{
						hit.transform.SendMessage("destroyThis");
					}
				
					else if (hit.transform.tag == "OrbDestroyer")
					{
						return;
					}
				
	            	else
					{
						orb = (GameObject)Instantiate(AttractionOrb, hit.point, rot);
						orbCount++;
						Destroy (orb, timeToLive);
						StartCoroutine(Timer());
					}
	    		}		
		}
		
				
		
		else if(Input.GetMouseButtonDown(1) && orbCount < maxOrbCount)
		{	
			audio.PlayOneShot(laser);
			
			//GameObject emptyOrb2 = (GameObject)Instantiate(EmptyOrb2, transform.position, Quaternion.identity);
			//emptyOrb2.rigidbody.velocity = transform.forward * FireSpeed;
			
			RaycastHit hit = new RaycastHit();
			if (Physics.Raycast(transform.position, transform.forward, out hit))
			{
        		var rot = Quaternion.FromToRotation(Vector3.up, hit.normal);
        		if (hit.transform.tag == "AttractionOrb")
				{
					hit.transform.SendMessage("destroyThis");
				}
				
				else if (hit.transform.tag == "OrbDestroyer")
				{
					return;
				}
				
            	else
				{
					orb = (GameObject)Instantiate(RepulsionOrb, hit.point, rot);
					orbCount++;
					Destroy (orb, timeToLive);
					StartCoroutine(Timer());
				}
    		}
		}
	}
	
	public void shootAttractionOrb (GameObject inputOrb)
	{
		orb = (GameObject)Instantiate(AttractionOrb, inputOrb.rigidbody.position - inputOrb.rigidbody.velocity * 0.02f, Quaternion.identity);
		orb.rigidbody.velocity = (inputOrb.rigidbody.velocity)/7;
		Destroy(inputOrb);
		Destroy(orb, timeToLive);
		StartCoroutine(Timer());
	}
	
	IEnumerator Timer()
	{
		yield return new WaitForSeconds(timeToLive);
		orbCount--;
	}
}