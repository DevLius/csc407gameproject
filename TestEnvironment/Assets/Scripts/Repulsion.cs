using UnityEngine;
using System.Collections.Generic;
 
public class Repulsion : MonoBehaviour 
{	
	public static float range = 1000;
	private Orb orb;
	public float Mass = 100;
	
	void Start()
	{
		orb = this.gameObject.GetComponent("Orb") as Orb;
	}
 
	void FixedUpdate () 
	{
		if(orb.isAttached == true)
		{
			Collider[] cols  = Physics.OverlapSphere(transform.position, range); 
			List<Rigidbody> rbs = new List<Rigidbody>();
	 
			foreach(Collider c in cols)
			{
				Rigidbody rb = c.attachedRigidbody;
				if(rb != null && rb != rigidbody && !rbs.Contains(rb))
				{
					rbs.Add(rb);
					Vector3 offset = -(transform.position - c.transform.position);
					rb.AddForce( offset / offset.magnitude * Mass);
				}
			}
		}
	}
}