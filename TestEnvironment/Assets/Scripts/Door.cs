using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour
{
    private bool locked;
    private ParticleSystem particles;

    void Awake()
    {
        try {
            particles = transform.parent.particleSystem;
        } catch { }
        SetLocked(true);
    }

    public void SetLocked(bool locked)
    {
        this.locked = locked;
        try {
            particles.enableEmission = !locked;
        } catch {}
    }

    void OnTriggerEnter(Collider other)
    {
        if (!locked && other.gameObject.tag == "MainCamera")
            ((Level)FindObjectOfType(typeof(Level))).DoorEntered(this);
    }
}