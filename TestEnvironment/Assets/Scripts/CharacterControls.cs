using UnityEngine;
using System.Collections;
 
[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (SphereCollider))]
 
public class CharacterControls : MonoBehaviour {
 
	public float accelerationMagnitude = 10.0f;
	//public float gravity = 10.0f;
	public float maxVelocity = 10.0f;
	private bool grounded = false;
 
 
 
	void Awake() {
	    rigidbody.freezeRotation = true;
	    rigidbody.useGravity = false;
	}
 
	void FixedUpdate () {
		
		if(Input.GetAxis("Mouse ScrollWheel") != 0f)
		{
			Light flashlight = this.gameObject.GetComponentInChildren<Light>();
			flashlight.intensity += Input.GetAxis ("Mouse ScrollWheel") / 10;
			flashlight.range += Input.GetAxis("Mouse ScrollWheel") * 100;
		}
		
	    if (grounded) {
	        // Calculate how fast we should be moving
	        Vector3 targetForce = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Jump"), Input.GetAxis("Vertical"));
			
			targetForce.x = Mathf.Round(targetForce.x);
			targetForce.z = Mathf.Round(targetForce.z);
			
			//Debug.Log (targetForce.x + " : " + targetForce.y + " : " + targetForce.z);
			
			if(targetForce != Vector3.zero)
			{
			
				if(Input.GetButton("Reverse"))
				{
					targetForce.y *= -1;
				}	
				
				Transform cam = Camera.main.transform;
				
		        targetForce = cam.TransformDirection(targetForce);
				//targetVelocity = transform.TransformDirection(targetVelocity);
				//targetVelocity = groundOrient.TransformDirection(targetVelocity);
		        targetForce *= accelerationMagnitude;
	 
		        // Apply a force that attempts to reach our target velocity
			    Vector3 velocity = rigidbody.velocity;
				
				if(Mathf.Abs(velocity.x) > maxVelocity)
				{
					targetForce.x = 0;
				}
				if(Mathf.Abs(velocity.y) > maxVelocity)
				{
					targetForce.y = 0;
				}
				if(Mathf.Abs(velocity.z) > maxVelocity)
				{
					targetForce.z = 0;
				}
				
				
				rigidbody.AddForce(targetForce);
			    //Vector3 velocityChange = (targetVelocity - velocity);
				/*Vector3 velocityChange = targetVelocity;
			    velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
			    velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
			    velocityChange.y = Mathf.Clamp(velocityChange.y, -maxVelocityChange, maxVelocityChange);
			    //rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);*/
			}
	    }
 
	    grounded = false;
	}
 
	void OnCollisionStay (Collision col) {
	    grounded = true;
	}
}