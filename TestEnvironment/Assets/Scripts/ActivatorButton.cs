using UnityEngine;
using System.Collections;

public class ActivatorButton : Activator
{

    //public float duration = -1; //unlimited duration when <0
	private bool activated;
	private Behaviour on_led;
    private Behaviour off_led;
    private Level level;
	
	
	void Start()
    {
        on_led = (Behaviour)GetComponent("Halo");
        off_led = (Behaviour)transform.FindChild("Halo").GetComponent("Halo");
        level = ((Level)FindObjectOfType(typeof(Level)));

        activated = false;
        on_led.enabled = activated;
        off_led.enabled = !activated;
    }
	
	override public void SetActivated(bool active)
    {
        if (activated != active)
        {
            activated = active;
            on_led.enabled = active;
            off_led.enabled = !active;
            level.ActivatorStateChanged(this, active);
            if (active)
                StartCoroutine(Deactivate());
        }
    }

    private IEnumerator Deactivate()
    {
        if (duration >= 0)
        {
            yield return new WaitForSeconds(duration);
            SetActivated(false);
        }
    }
	
	
    override public void InteractionStarted()
    {
        SetActivated(!activated);
    }

}