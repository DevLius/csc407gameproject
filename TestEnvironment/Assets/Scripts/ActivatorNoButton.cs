using UnityEngine;
using System.Collections;

public class ActivatorNoButton : Activator
{


	private bool activated;
    private Behaviour halo1;
    private Behaviour halo2;
    private Level level;
	
	
	void Start()
    {
        halo1 = (Behaviour)GetComponent("Halo");
        halo2 = (Behaviour)transform.FindChild("Halo").GetComponent("Halo");
        level = ((Level)FindObjectOfType(typeof(Level)));

        activated = false;
        halo1.enabled = activated;
        halo2.enabled = !activated;
    }
	
	void OnTriggerEnter()
	{
		if (!activated)
			SetActivated(!activated);
	}
	
	
	override public void SetActivated(bool active)
    {
        if (activated != active)
        {
            activated = active;
            halo1.enabled = active;
            halo2.enabled = !active;
            level.ActivatorStateChanged(this, active);
            if (active)
                StartCoroutine(Deactivate());
        }
    }

    private IEnumerator Deactivate()
    {
        if (duration >= 0)
        {
            yield return new WaitForSeconds(duration);
            SetActivated(false);
        }
    }
	
    override public void InteractionStarted()
    {
    }

}