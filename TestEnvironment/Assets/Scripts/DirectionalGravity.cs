using UnityEngine;
using System.Collections;
using System;

public class DirectionalGravity : MonoBehaviour {
	
	//public Vector3 gravityDirection = Vector3.one;
	public Vector3 gravityDirection;
	public float Mass = 100;
	
	//private MeshFilter filter;
	//private Vector3 planeNormal;

	// Use this for initialization
	void Start () {
		//filter = (MeshFilter)this.gameObject.GetComponent("MeshFilter");
		//planeNormal = filter.transform.TransformDirection(filter.mesh.normals[0]);
		gravityDirection = -transform.up;
	}
	
	void OnTriggerEnter(Collider other)
	{
		//Vector3 planeToPoint = other.transform.position - transform.position;
		
		//float offset = Vector3.Dot(planeNormal, planeToPoint)/planeNormal.magnitude;
		
        try {
            other.rigidbody.AddForce(gravityDirection * Mass);
        } catch(MissingComponentException) {}
	}
	
	void OnTriggerStay(Collider other)
	{
		//Vector3 planeToPoint = other.transform.position - transform.position;
		
		//float offset = Vector3.Dot(planeNormal, planeToPoint)/planeNormal.magnitude;
        try {
            other.rigidbody.AddForce(gravityDirection * Mass);
        } catch(MissingComponentException) {}
	}
}
