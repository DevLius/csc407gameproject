using UnityEngine;
using System.Collections;

public class SphereMovement : MonoBehaviour {
	
	public float speed = 10;

	// Use this for initialization
	void Start () {
		this.rigidbody.velocity = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)) * speed;
	}
}
