using UnityEngine;
using System.Collections;

public class SupplyR : InteractiveObject {

    public string message = "";
    public float duration = 0;

    private char[] splitter = {'|'};
    private string finalMessage = "Log:";

    void Start()
    {
        foreach (string message in this.message.Split(splitter))
            finalMessage += "\n" + message;
    }

    override public void InteractionStarted()
    {
        ((Level)FindObjectOfType(typeof(Level))).ShowMessage(finalMessage, duration);
    }
}
