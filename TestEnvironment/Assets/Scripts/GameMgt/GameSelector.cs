using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSelector : MonoBehaviour
{
    public GUISkin guiSkin;

    private List<string> games;
    private Vector2 scrollVector;
    private string newGameName;

    //recovers all the available games and stores them in memory
    void Start()
    {
        //PlayerPrefs.DeleteAll(); 
        int gameNo = 0;

        games = new List<string>();
        scrollVector = Vector2.zero;
        newGameName = "";

        while(PlayerPrefs.HasKey("Game"+gameNo))
            games.Add(PlayerPrefs.GetString("Game"+gameNo++));
    }

	void OnGUI()
    {
        GUI.skin = guiSkin;
        int gameNo = 0;

        GUI.Label(new Rect(Screen.width / 2 - 715, Screen.height / 2 - 380, 600, 150), "Profile");
		GUI.BeginGroup(new Rect(Screen.width / 2 - 565, Screen.height / 2 - 290, 600, 500)); //Begin menu container
		
		if (games.Count != 0) {
			if (games.Count <= 3) {
				foreach (string game in games)
	            if (GUI.Button(new Rect(0, gameNo++ * 53 + 60, 284, 50), game))
	                StartGame("Game" + (gameNo-1));		
				
			}
			else {
		                                                                                  //for some reason, the content's heigh must to be fixed
		        scrollVector = GUI.BeginScrollView(new Rect(0, 60, 300, 158), scrollVector, new Rect(0, 0, 284, 318), false, true);
		        foreach (string game in games)
		            if (GUI.Button(new Rect(0, gameNo++ * 53, 284, 50), game))
		                StartGame("Game" + (gameNo-1));
		        GUI.EndScrollView();
			}
		}
		int tempHeight;
		if (games.Count <= 3)
			tempHeight = games.Count * 53 + 125;
		else tempHeight = 280;
        newGameName = GUI.TextField(new Rect(0, tempHeight, 220, 50), newGameName);
        if (GUI.Button(new Rect(235, tempHeight, 75, 50), "New") && newGameName!="")
            NewGame();
		GUI.EndGroup(); //End menu container


        
	}

    private void NewGame()
    {
        PlayerPrefs.SetString("Game"+games.Count, newGameName);
        PlayerPrefs.SetInt("Game"+games.Count+"difficulty", (int) Difficulties.NORMAL); //Default difficulty: NORMAL
        StartGame("Game" + games.Count);
    }
	
	private void StartGame(string gameID)
    {
        DontDestroyOnLoad(GameManager.Instance); // DO NOT DESTORY WHEN LOADING A NEW SCENE
		GameManager.Instance.StartGame(gameID);
	}
}
