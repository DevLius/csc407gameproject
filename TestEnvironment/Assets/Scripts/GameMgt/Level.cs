using UnityEngine;
using System.Collections;

public abstract class Level : MonoBehaviour
{
    public GUISkin guiSkin;

    private Player player;
    private bool showMenu;
    private float messageTimeLeft;
    private string message;
    private bool levelFinished;
    private bool playerDead;

    protected int difficultyRate; //int ~ difficulty (starting in 0), don't modify this value, just read it
    protected int score;


    //Set level difficulty, used Awake instead of Start to let the specialized classes have their own Start
    void Awake()
    {
        player = (Player)FindObjectOfType(typeof(Player));
        difficultyRate = (int) GameManager.Instance.getDifficulty();

        showMenu = false;
        messageTimeLeft = 0;
        levelFinished = false;
        playerDead = false;
        score = 0;
        Screen.showCursor = false;

        player.energyDrainingSpeed = difficultyRate * .8F;
        player.maxVelocityBeforeDamage = 21 - difficultyRate * 2.5F;
        player.maxVelocityBeforeDeath = 70 - difficultyRate * 8; //Sets the whole value because it's in the Awake method (can't assume it's already initialized)
    }


    //Display an informative message <message> for <time> seconds
    public void ShowMessage(string message, float time)
    {
        this.message = message;
        messageTimeLeft = time;
    }

    void OnGUI()
    {
        GUI.skin = guiSkin;

        if (showMenu) //Show in game menu
        {
            GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 200, 400, 400), ""); //Menu background
            GUI.BeginGroup(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 300)); //Menu container

            GUI.Label(new Rect(0, 0, 300, 100), Application.loadedLevelName); //Title of the level
            if (GUI.Button(new Rect(0, 120, 300, 50), "Resume"))
            {
				Screen.showCursor = false;
                showMenu = false;
                Time.timeScale = 1;
                player.GetComponent<OrbFiring>().enabled = true;
                player.transform.FindChild("Gun").FindChild("Laser").GetComponent<Laser>().enabled = true;
                player.GetComponent<MouseLook2>().enabled = true;
            }
            if (GUI.Button(new Rect(0, 180, 300, 50), "Restart Level"))
            {
				Screen.showCursor = false;
                Time.timeScale = 1;
                Application.LoadLevel(Application.loadedLevel);
            }
            if (GUI.Button(new Rect(0, 240, 300, 50), "Return to Main Menu"))
            {
                Time.timeScale = 1;
                GameManager.Instance.LevelAborted();
            }

            GUI.EndGroup(); //End menu container
        }
        else if (levelFinished) //Show level finished screen
        {
            GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 200, 400, 400), ""); //Screen background
            GUI.BeginGroup(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 300)); //Screen container

            GUI.TextField(new Rect(0, 0, 400, 100), "Mission Accomplished!");
            GUI.TextField(new Rect(0, 100, 400, 100), "Difficulty: " + GameManager.Instance.difficultiesName[(int)GameManager.Instance.getDifficulty()]);
            GUI.TextField(new Rect(0, 200, 400, 100), "Score: " + score);

            GUI.EndGroup(); //Screen container
        }
        else if (playerDead) //Show player dead screen
        {
            GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 200, 400, 400), ""); //Screen background
            GUI.BeginGroup(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 150, 300, 300)); //Screen container

            GUI.Label(new Rect(0, 0, 300, 100), "Game Over");
            GUI.Label(new Rect(0, 200, 300, 100), Application.loadedLevelName);

            GUI.EndGroup(); //Screen container
        }
        else
        {
            if (Time.timeSinceLevelLoad <= 5) //Show the title of the level for the first 5 seconds
            {
                GUI.Box(new Rect(Screen.width / 2 - 300, 0, 600, 100), "");
                GUI.Label(new Rect(Screen.width / 2 - 250, 0, 500, 100), Application.loadedLevelName);
            }

            if (messageTimeLeft > 0) //Show the message requested through the method ShowMessage
            {
				GUI.Box(new Rect(Screen.width - 310, 5, 305, 130), "");
                GUI.TextArea(new Rect(Screen.width - 305, 10, 300, 120), message);
            }
        }

    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        { //It's recommended to call input methods in the update method
			Screen.showCursor = !Screen.showCursor;

            player.GetComponent<OrbFiring>().enabled = showMenu;
            player.transform.FindChild("Gun").FindChild("Laser").GetComponent<Laser>().enabled = showMenu;
            player.GetComponent<MouseLook2>().enabled = showMenu;
            showMenu = !showMenu;
            Time.timeScale = (showMenu ? 0 : 1);
        }

        if (messageTimeLeft > 0)
            messageTimeLeft -= Time.deltaTime;
    }



    private IEnumerator Timer(float timeScale, float realTimeTiming)
    {
        Time.timeScale = timeScale;
        yield return new WaitForSeconds(timeScale * realTimeTiming);
        Time.timeScale = 1;
    }

    //Show the level completed screen and triggers the notification to GameManager (the actual notification is done in the Update method)
    //Note: This method is called by the level script's specializations when they know the level is over
    public void FinishLevel()
    {
        if (!playerDead && !levelFinished)
        { //Makes sure the player can't finish the game... dead!!, and can't win more than once
            levelFinished = true;
            score += (int)Mathf.Round(player.GetEnergyLevel());
            StartCoroutine(NextLevel());
        }
    }

    private IEnumerator NextLevel()
    {
        yield return StartCoroutine(Timer(.35F, 5));
        GameManager.Instance.LevelFinished(score);
    }

    //Shows a game over screen and triggers the restart of the level (the actual restart is done in the Restart method)
    //Note: This method is called by Player when it dies
    public void PlayerDead()
    {
        if (!playerDead && !levelFinished)
        { //Protection against multiple calls to the method
            playerDead = true;
            StartCoroutine(Restart());
        }
    }

    private IEnumerator Restart()
    {
        yield return StartCoroutine(Timer(.35F, 5));
        Application.LoadLevel(Application.loadedLevel);
    }



    //Takes an action depending on the interrupter activated and the level's puzzle
    abstract public void ActivatorStateChanged(Activator activator, bool activated);

    //Takes an action depending on the door entered and the level's puzzle
    abstract public void DoorEntered(Door door);
}