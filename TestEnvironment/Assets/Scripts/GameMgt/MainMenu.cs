using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour {

    public GUISkin guiSkin;

    private Difficulties difficulty;

    private Vector2 lvlsScroll;
    private bool chooseLevel;
    private bool chooseDifficulty;

    private List<int[]> levelsPlayed;
    public Texture2D[] levelPreviews; //Levels preview, 200 x 200

    void Start()
    {
        int levelIndex = 0;

        lvlsScroll = Vector2.zero;
        chooseLevel = false;
        chooseDifficulty = false;

        difficulty = GameManager.Instance.getDifficulty();
        levelsPlayed = new List<int[]>();

        while (PlayerPrefs.HasKey(GameManager.Instance.getGameID() + "Level" + levelIndex + "difficulty")) //Retrieves levels played info
            levelsPlayed.Add(new int[2] {
                PlayerPrefs.GetInt(GameManager.Instance.getGameID() + "Level" + levelIndex + "difficulty"),
                PlayerPrefs.GetInt(GameManager.Instance.getGameID() + "Level" + levelIndex++ + "score") } );
    }


    void OnGUI()
    {
        int deltaY = 0;
        int levelIndex = 0;
        GUI.skin = guiSkin;
        GUIStyle lvlButton = new GUIStyle(GUI.skin.button); //Style for the level buttons
        lvlButton.imagePosition = ImagePosition.ImageAbove;

        GUI.Label(new Rect(Screen.width / 2 - 715, Screen.height / 2 - 380, 600, 150), "YOU"); //Title
        GUI.BeginGroup(new Rect(Screen.width / 2 - 565, Screen.height / 2 - 290, 600, 700));  //Menu container

        if (levelsPlayed.Count == 0) {
			if (GUI.Button(new Rect(0, 60, 300, 50), "Start Playing"))
				GameManager.Instance.StartLevel(3, difficulty);
            	//GameManager.Instance.StartLevel(levelsPlayed.Count-(AvailableNonPlayedLvls() ? 0:1), difficulty);
		}
		else {
			if (GUI.Button(new Rect(0, 60, 300, 50), "Continue"))
            	GameManager.Instance.StartLevel(levelsPlayed.Count-(AvailableNonPlayedLvls() ? 0:1), difficulty);
		}

        chooseLevel = GUI.Toggle(new Rect(0, 120, 300, 50), chooseLevel, "Levels Available", "button");
        if (chooseLevel)
        {
			chooseDifficulty = false;
			
            deltaY += 220;
            lvlsScroll = GUI.BeginScrollView(new Rect(0, 170, 500, 230), lvlsScroll, new Rect(0, 0,
                        (levelsPlayed.Count + (AvailableNonPlayedLvls() ? 1 : 0)) * 205, 220)); //Begin level selector

            foreach (int[] best in levelsPlayed) //Already finished levels
                if (GUI.Button(new Rect(levelIndex * 205, 0, 200, 220), new GUIContent(LevelName(levelIndex) + "\n " + GameManager.Instance.difficultiesName[best[0]] + " " + best[1] +"'", levelPreviews[levelIndex], LevelName(levelIndex++)), lvlButton))
					GameManager.Instance.StartLevel(levelIndex-1, difficulty);

            if (AvailableNonPlayedLvls() && GUI.Button(new Rect(levelIndex * 205, 0, 200, 220), new GUIContent(LevelName(levelIndex) + "\n<New>", levelPreviews[levelIndex], LevelName(levelIndex)), lvlButton)) //Newest available level
                GameManager.Instance.StartLevel(levelIndex, difficulty);

            GUI.EndScrollView(); //End level selector
        }

        chooseDifficulty = GUI.Toggle(new Rect(0, 180 + deltaY, 300, 50), chooseDifficulty, "Difficulty", "button");
        if (chooseDifficulty)
        {
			chooseLevel = false;
            difficulty = (Difficulties) GUI.Toolbar(new Rect(0, 230 + deltaY, 500, 50), (int) difficulty, GameManager.Instance.difficultiesName);
            deltaY += 50;
        }

        if (GUI.Button(new Rect(0, 280 + deltaY, 300, 50), "Quit"))
            GameManager.Instance.ExitGame(difficulty);

        GUI.EndGroup(); // End menu container
    }


    //Checks if there are more levels than the ones already played
    private bool AvailableNonPlayedLvls() {
        return GameManager.Instance.tutorialIndex + levelsPlayed.Count < Application.levelCount;
    }

    //Returns the name of the level based on its index
    private string LevelName(int levelIndex)
    {
        string levelName = this.gameObject.GetComponent<ReadSceneNames>().scenes[levelIndex + GameManager.Instance.tutorialIndex];
        //levelName = levelName.Substring(levelName.LastIndexOf('/') + 1);
        //return levelName.Substring(0, levelName.Length - 6);
		return levelName;
    }

}
