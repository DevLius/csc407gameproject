using UnityEngine;
using System.Collections;

public enum Difficulties { VERYEASY, EASY, NORMAL, DIFFICULT, VERYDIFFICULT };

public class GameManager : MonoBehaviour {
	private static GameManager instance; // c# property

	private string gameID;
    private string gameName;
	private Difficulties difficulty; //The last choosen difficulty
    //Application.loadedLevel contains the actual level

    public readonly int tutorialIndex = 2; //The index of the scene containing the tutorial
    public string[] difficultiesName = { "Easy...", "Dumb", "Normal", "Smart", "Genius" };
	


	// create singleton game state
	public static GameManager Instance {
		get {
			if (instance == null)
				instance = new GameObject("GameManager").AddComponent<GameManager>();
			return instance;
		}
	}
	

	
    //Used by the game selector to start the game
	public void StartGame(string gameID)
    {
		this.gameID = gameID;
        gameName = PlayerPrefs.GetString(gameID);
		difficulty = (Difficulties) PlayerPrefs.GetInt(gameID+"difficulty");
		Application.LoadLevel("MainMenu");
	}

    //Used by the main menu to start each level
    public void StartLevel(int levelIndex, Difficulties difficulty)
    {
		Screen.showCursor = false;

        this.difficulty = difficulty;

        Application.LoadLevel(tutorialIndex + levelIndex); //The existence of the level is already validated
    }

    //Used by the levels to inform that have been accomplished succesfully
    public void LevelFinished(int score)
    {
        //Update level stats
        if (!PlayerPrefs.HasKey(gameID + "Level" + (Application.loadedLevel - tutorialIndex) + "difficulty") ||
            PlayerPrefs.GetInt(gameID + "Level" + (Application.loadedLevel - tutorialIndex) + "difficulty") < (int)difficulty)
        {
            PlayerPrefs.SetInt(gameID + "Level" + (Application.loadedLevel - tutorialIndex) + "difficulty", (int)difficulty);
            PlayerPrefs.SetInt(gameID + "Level" + (Application.loadedLevel - tutorialIndex) + "score", score);
        }
        else if (PlayerPrefs.GetInt(gameID + "Level" + (Application.loadedLevel - tutorialIndex) + "difficulty") == (int)difficulty
            && PlayerPrefs.GetInt(gameID + "Level" + (Application.loadedLevel - tutorialIndex) + "score") < score )
                PlayerPrefs.SetInt(gameID + "Level" + (Application.loadedLevel - tutorialIndex) + "score", score);
        
        //Load next level, if exists, otherwise go to the main menu
        if (Application.levelCount > Application.loadedLevel + 1)
            Application.LoadLevel(Application.loadedLevel + 1);
        else
            Application.LoadLevel("MainMenu");
    }

    //Used by the levels to inform that the user has left through the in game menu
    public void LevelAborted()
    {
        Application.LoadLevel("MainMenu");
    }

    //Used by the main menu to exit the game
    public void ExitGame(Difficulties difficulty)
    {
        instance = null;
        PlayerPrefs.SetInt(gameID + "difficulty", (int)difficulty);
        Application.Quit();
    }



    public string getGameID() {
        return gameID;
    }

    public string getGameName() {
        return gameName;
    }

    public Difficulties getDifficulty() {
        return difficulty;
    }
}
