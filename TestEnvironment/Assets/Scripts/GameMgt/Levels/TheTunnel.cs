using UnityEngine;
using System.Collections;

public class TheTunnel : Level {
	
	public Door door;

	// Use this for initialization
	void Start () {
		door.SetLocked(false);
		ShowMessage("Wow it's dark in here!\n Maybe I should use the ScrollWheel to turn up my flashlight", 8);
	}
	
	override public void ActivatorStateChanged(Activator activator, bool activated)
    {
    }
	
	override public void DoorEntered(Door door)
	{
		score += 10 * (2 - FindObjectsOfType(typeof(EnergyCapsule)).Length);
        FinishLevel();
	}
	
}