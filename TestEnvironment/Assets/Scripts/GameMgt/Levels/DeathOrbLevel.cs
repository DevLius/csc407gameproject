using UnityEngine;
using System.Collections;

public class DeathOrbLevel : Level {
	
	public Door door;
	private bool doorWasActivated;
	public Activator doorActivator;
	public float speed = 25;
	
	void Start () {
        doorActivator.duration = 15 - difficultyRate;
		doorWasActivated = false;
	}
	
	override public void ActivatorStateChanged(Activator activator, bool activated)
    {
        door.SetLocked(!activated);
		
		/*if(!doorWasActivated)
		{
			foreach(SphereMovement sphereMov in FindObjectsOfType(typeof(SphereMovement)))
			{
				sphereMov.gameObject.rigidbody.velocity = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)) * speed;
			}
			doorWasActivated = true;
		}*/
    }
	
	override public void DoorEntered(Door door)
	{
		score += 10 * (2 - FindObjectsOfType(typeof(EnergyCapsule)).Length);
        FinishLevel();
	}
	
}