using UnityEngine;
using System.Collections;

public class TestingBox : Level
{

    public Door lastDoorOfLevel; //This is not necessary in this case, it's just to demonstrate an example
    public Activator lastDoorActivator;

    void Start()
    {
        lastDoorActivator.duration = 15 - 2*difficultyRate;
        ShowMessage("An example of how the level script generalizes and thus makes things easier", 3);
    }

    override public void ActivatorStateChanged(Activator activator, bool activated)
    {
        if (activator == lastDoorActivator)
            lastDoorOfLevel.SetLocked(!activated);
    }

    override public void DoorEntered(Door door)
    {
        if (lastDoorOfLevel == door)
        {
            score += 10 * (1 - FindObjectsOfType(typeof(EnergyCapsule)).Length);//A general way to get how many capsules were taken
            FinishLevel();
        }
    }

}
