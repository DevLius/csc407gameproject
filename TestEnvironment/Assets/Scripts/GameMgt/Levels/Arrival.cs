using UnityEngine;
using System.Collections;

public class Arrival : Level {

    public Activator doorActivator;
    public Door door;
    public Door checkPoint1, checkPoint2, checkPoint3, checkPoint4;
    public GameObject deathOrb;

    private Vector3 deathOrbStartPosition = new Vector3(58.5F, 9.5F, -108.2F);

	void Start () {
        doorActivator.duration = 15 - difficultyRate;
        ShowMessage("Scanning systems ... \nEverything working properly. \nBattery full and generator working."+
            "\n\nGravity guns test started: Left click to shoot an atractive orb, right click for a repulsive one", 8);
        checkPoint1.SetLocked(false);

        if (difficultyRate >= (int)Difficulties.NORMAL)
            StartCoroutine(RandomDeathOrb());
	}

    IEnumerator RandomDeathOrb()
    {
        yield return new WaitForSeconds(Random.Range(15 - (difficultyRate-(int)Difficulties.NORMAL) * 5, 20));
        deathOrb.rigidbody.AddForce(Random.Range(1, 9), Random.Range(1, 9), Random.Range(1, 9));
        Instantiate(deathOrb, deathOrbStartPosition, Quaternion.identity);
        StartCoroutine(RandomDeathOrb());
    }
	
	override public void ActivatorStateChanged(Activator activator, bool activated)
    {
        door.SetLocked(!activated);
    }

    override public void DoorEntered(Door door) {
        if (door == checkPoint1) {
            ShowMessage("Seems that the gravity fields are not working ...", 4);
            Destroy(checkPoint1.gameObject);
            checkPoint2.SetLocked(false);
        }
        else if (door == checkPoint2) {
            ShowMessage("Possible energy source identified: Yellow sphere \n\n" +
                        "What is that? I could access the main computer's registry from that SupplyR machine ...", 8);
            Destroy(checkPoint2.gameObject);
            checkPoint3.SetLocked(false);
        }
        else if (door == checkPoint3) {
            ShowMessage("Well, now it's a fact, the synthetic gravity fields are not working properly ...", 4);
            Destroy(checkPoint3.gameObject);
            checkPoint4.SetLocked(false);
        }
        else if (door == checkPoint4) {
            ShowMessage("Confirmed, all systems got crazy.\nI wonder what is that activator for ...", 4);
            Destroy(checkPoint4.gameObject);
        }
        else {
            score += 10 * (2 - FindObjectsOfType(typeof(EnergyCapsule)).Length);
            FinishLevel();
        }
    }

}
