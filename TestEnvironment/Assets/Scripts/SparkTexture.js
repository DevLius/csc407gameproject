var uvAnimationTileX = 5; //Here you can place the number of columns of your sheet. 
                           //The above sheet has 24
 
var uvAnimationTileY = 10; //Here you can place the number of rows of your sheet. 
                          //The above sheet has 1
var framesPerSecond = 30;

var duration = 1.0; // duration of the sparking effect.

var lack_duration = 1.0; //time between sparks

private var lack_aux = lack_duration;
private var duration_aux = duration;

 
function Update () {
    if(duration_aux > 0)
    {
        // Calculate index
        var index : int = Time.time * framesPerSecond;
        // repeat when exhausting all frames
        index = index % (uvAnimationTileX * uvAnimationTileY);
	 
        // Size of every tile
        var size = Vector2 (1.0 / uvAnimationTileX, 1.0 / uvAnimationTileY);
	 
        // split into horizontal and vertical index
        var uIndex = index % uvAnimationTileX;
        var vIndex = index / uvAnimationTileX;
	 
        // build offset
        // v coordinate is the bottom of the image in opengl so we need to invert.
        var offset = Vector2 (uIndex * size.x, 1.0 - size.y - vIndex * size.y);
	 
        renderer.material.SetTextureOffset ("_MainTex", offset);
        renderer.material.SetTextureScale ("_MainTex", size);

        duration_aux -= Time.deltaTime;
    }
    else if(lack_aux > 0)
    {
        if(lack_aux == lack_duration)
            renderer.enabled = false;
        lack_aux -= Time.deltaTime;
    }
    else
    {
        lack_aux = lack_duration;
        duration_aux = duration;
        renderer.enabled = true;
    }
}