using UnityEngine;  
using System.Collections;  
  
  
public class Movie : MonoBehaviour  
{  
	public MovieTexture movie;
  
    void Start()  
    {  
		movie = (MovieTexture)Resources.Load("story");
		movie.Play();
    }  
	
	void onGUI (){
		if (movie.isPlaying == false) Application.LoadLevel(0);
	}
}  
