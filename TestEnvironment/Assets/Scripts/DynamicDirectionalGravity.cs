using UnityEngine;
using System.Collections;

public class DynamicDirectionalGravity : MonoBehaviour {
	
	public float Mass;
	
	void OnTriggerEnter(Collider other)
	{
		DirectionalGravity diGrav = this.GetComponentInChildren<DirectionalGravity>();
		
		if(other.gameObject.tag == "AttractionOrb")
		{
			diGrav.Mass = Mass;
			diGrav.gravityDirection = -transform.up;
		}
		else if(other.gameObject.tag == "RepulsionOrb")
		{
			diGrav.Mass = Mass;
			diGrav.gravityDirection = transform.up;
		}
	}
}
