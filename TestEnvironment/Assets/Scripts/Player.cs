using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	public bool dead;
	public float energyDrainingSpeed = 1.0f;
	private float energyLevel;
	public float maxEnergy = 100;
	public float maxVelocityBeforeDamage = 10f;
	public float maxVelocityBeforeDeath = 30f;
	public GUIStyle energyBarStyle;
	private Texture2D energyIcon;
	private AudioClip collideSound;
	private AudioClip laserCol;


	// Use this for initialization
	void Start () {
		energyIcon = (Texture2D) Resources.Load("bar");
		dead = false;
		energyLevel = maxEnergy;
		energyBarStyle.normal.background = (Texture2D)Resources.Load("energy_healthy");
		collideSound = (AudioClip)Resources.Load("crash2");
		laserCol = (AudioClip)Resources.Load("crash2");
		
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		energyLevel -= Time.deltaTime * energyDrainingSpeed;
		energyLevel = Mathf.Clamp(energyLevel, 0, maxEnergy);
				
		if(energyLevel == 0)
		{
			dead = true;
		}
		if (energyLevel < 40) {
			energyBarStyle.normal.background = (Texture2D)Resources.Load("energy_alert");
		}
		else {
			energyBarStyle.normal.background = (Texture2D)Resources.Load("energy_healthy");
		}
		
		if(dead)
		{
			//Application.LoadLevel(Application.loadedLevel);
            ((Level) FindObjectOfType(typeof(Level))).PlayerDead();
		}
	}
	
	void OnGUI() {
		GUI.DrawTexture(new Rect(5,5, 349, 114), energyIcon);
		GUI.Box (new Rect(118, 34, energyLevel * 2, 26), "", energyBarStyle);
	}
	
	void OnCollisionEnter(Collision col){
		Collider other = col.collider;
		
		if (other.gameObject.tag == "Laser") {
			audio.PlayOneShot(collideSound);
		}
		
		if(other.isTrigger == false)
		{
			/*Vector3 velocityDifference;
			if(other.rigidbody != null)
			{
				velocityDifference = this.rigidbody.velocity - other.rigidbody.velocity;
			}
			else
			{
				velocityDifference = this.rigidbody.velocity;
			}*/
			
			float minDot = Mathf.Abs (Vector3.Dot(col.relativeVelocity, col.contacts[0].normal));
			Vector3 minVect = col.contacts[0].normal;
			foreach(ContactPoint contact in col.contacts)
			{
				if(Mathf.Abs(Vector3.Dot(col.relativeVelocity, contact.normal)) < minDot)
				{
					minDot = Mathf.Abs(Vector3.Dot(col.relativeVelocity, contact.normal));
					minVect = contact.normal;
				}
			}
			
			Vector3 impactVelocity = Vector3.Project(col.relativeVelocity, minVect);
			
			//Debug.Log (this.rigidbody.velocity.sqrMagnitude);
			//Debug.Log ("Velocity Difference: " + velocityDifference.magnitude);
			//Debug.Log ("Relative Velocity: " + col.relativeVelocity.magnitude);
			//Debug.Log(impactVelocity.magnitude);
			if(impactVelocity.sqrMagnitude > Mathf.Pow(maxVelocityBeforeDamage, 2))
			{
				
				energyLevel -= (maxEnergy/(maxVelocityBeforeDeath - maxVelocityBeforeDamage)) * (impactVelocity.magnitude - maxVelocityBeforeDamage);
				if (other.gameObject.tag == "ColSound") {
					audio.PlayOneShot(collideSound);
				}
			}
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		if(other.gameObject.tag == "RegenStation")
		{
			energyLevel += Time.deltaTime * 10;
			energyLevel = Mathf.Clamp(energyLevel, 0, maxEnergy);
		}
	}
	
	void addEnergy(int energy)
	{
		energyLevel += energy;
		energyLevel = Mathf.Clamp(energyLevel, 0, maxEnergy);

	}

    public float GetEnergyLevel()
    {
        return energyLevel;
    }
}